post_rpm_install_append_class-native() {
	create_cmdline_wrapper ${D}/${bindir}/file \
		--magic-file ${datadir}/misc/magic.mgc
}

PROVIDES_append_class-native = " file-replacement-native"
