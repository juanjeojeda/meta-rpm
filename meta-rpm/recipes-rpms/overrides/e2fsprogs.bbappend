post_rpm_install_append_class-native() {
        for i in mke2fs mkfs.ext2 mkfs.ext3 mkfs.ext4; do
                create_wrapper ${D}${sbindir}/$i MKE2FS_CONFIG=${sysconfdir}/mke2fs.conf
        done
}
