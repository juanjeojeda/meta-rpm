GCCBINV = "8"

gcc_fixup_libgcc_so () {
  if [ -L ${D}${GCCDIR}/libgcc_s.so ]; then
    # On aarch64 this is a symlink to /lib64 which isn't in the sysroot, make it in /usr/lib64 instead which is
    # This will be made relative later
    LNK=$(readlink ${D}${GCCDIR}/libgcc_s.so)
    ln -sf /usr$LNK ${D}${GCCDIR}/libgcc_s.so
  else
    # On x86_64 this is a linker script which we want to tweak to not use an absolute path
    sed -i 's@/lib.*/libgcc_s.so.1@libgcc_s.so.1@g' ${D}${GCCDIR}/libgcc_s.so
    if [ -f ${D}${GCCDIR}/32/libgcc_s.so ]; then
      sed -i 's@/lib.*/libgcc_s.so.1@libgcc_s.so.1@g' ${D}${GCCDIR}/32/libgcc_s.so
    fi
  fi
}
