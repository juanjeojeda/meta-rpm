post_rpm_install_append_class-native() {
        # We need these from host-tool, so the setuid bit works
        rm ${D}/${bindir}/newuidmap ${D}/${bindir}/newgidmap
}
