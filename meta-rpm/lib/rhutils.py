import struct, lzma, gzip, shutil
from collections import namedtuple

RPM_HEADER_MAGIC = b'\x8e\xad\xe8\x01\x00\x00\x00\x00'
RPM_ENTRY_INFO_SIZE = 16
RPMTAG_PAYLOADCOMPRESSOR = 1125
HEADER_SIGBASE = 256
RPMTAG_SIG_BASE = HEADER_SIGBASE
RPMTAG_ARCHIVESIZE = 1046
RPMTAG_LONGARCHIVESIZE = RPMTAG_SIG_BASE+15
RPMTAG_LONGFILESIZES = 5008
RPMSIGTAG_PAYLOADSIZE = 1007
RPM_STRING_TYPE = 6

RpmLead = namedtuple('RpmLead', 'magic major minor type archnum name osnum signature_type')
RpmHeaderEntry = namedtuple('RpmHeaderEntry', 'tag type offset count')

def rpm_read_header(has_magic, is_signatures, f):
    if has_magic:
        header_len = 4*4
        header_raw = f.read(header_len)
        header = struct.unpack("!8sll", header_raw)
        assert header[0] == RPM_HEADER_MAGIC
        header = header[1:]
    else:
        header_len = 2*4
        header_raw = f.read(header_len)
        header = struct.unpack("!ll", header_raw)

    num_entries = header[0]
    data_length = header[1]

    header_len = header_len + num_entries * RPM_ENTRY_INFO_SIZE + data_length

    entries = []
    for i in range (num_entries):
        entry_raw = f.read(RPM_ENTRY_INFO_SIZE)
        entry = RpmHeaderEntry._make(struct.unpack("!lLlL", entry_raw))
        entries.append(entry)

    data = f.read(data_length)

    if is_signatures:
        pad = (8 - (header_len % 8)) % 8;
        if pad != 0:
            f.read(pad)

    return (entries, data)

def rpm_get_header(hdr, tag):
    entries = hdr[0]
    for e in entries:
        if e.tag == tag:
            return e
    return None

def rpm_get_header_string(hdr, tag):
    e = rpm_get_header(hdr, tag)
    if not e:
        return None
    assert e.count == 1
    assert e.type == RPM_STRING_TYPE
    data = hdr[1]
    end = e.offset
    while data[end] != 0:
        end = end + 1
    return data[e.offset:end].decode("utf-8", "ignore")

    return struct.unpack("!" + e_format, data[e.offset:e.offset+e_size])[0]


# We reimplement rpm2cpio to avoid cyclic dependencies on rpm
def rpm2cpio(path):

    f = open(path, "rb")


    raw_lead = f.read(96)
    lead = RpmLead._make(struct.unpack("!4scchh66shh16x", raw_lead))
    if lead.magic != b'\xed\xab\xee\xdb':
        raise Exception("Not an rpm")

    sig_hdr = rpm_read_header(True, True, f)
    hdr = rpm_read_header(True, False, f)
    compr = rpm_get_header_string(hdr, RPMTAG_PAYLOADCOMPRESSOR)
    if compr == "xz":
        f_decompressed = lzma.LZMAFile(f)
    else:
        f_decompressed = gzip.GzipFile(fileobj=f)

    return f_decompressed


def split_rpm_filename(filename):
    if filename[-4:] == '.rpm':
        filename = filename[:-4]
    arch_index = filename.rfind('.')
    arch = filename[arch_index+1:]
    rel_index = filename[:arch_index].rfind('-')
    rel = filename[rel_index+1:arch_index]
    ver_index = filename[:rel_index].rfind('-')
    ver = filename[ver_index+1:rel_index]
    epochIndex = filename.find(':')
    if epochIndex == -1:
        epoch = ''
    else:
        epoch = filename[:epochIndex]
    name = filename[epochIndex + 1:ver_index]
    return name, ver, rel, epoch, arch


# Converts rpm style requirement "foo >= 1" to yocto style rdep "foo (>= 1)"
def rpm_req_to_rdep(req):
    space = req.find(" ")
    if space > 0:
       return "%s (%s)" % (req[:space], req[space+1:])
    return req # No requirements
