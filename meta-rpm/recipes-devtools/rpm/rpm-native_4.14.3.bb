include rpm-native.inc

# Based on 4.14.3-13

SRC_URI = "http://ftp.rpm.org/releases/rpm-4.14.x/rpm-4.14.3.tar.bz2 \
           file://environment.d-rpm.sh \
           file://rpm-4.14.2-no-short-circuit.patch \
           file://0001-Do-not-read-config-files-from-HOME.patch \
           file://0002-Add-support-for-prefixing-etc-from-RPM_ETCCONFIGDIR-.patch \
           file://0011-Do-not-require-that-ELF-binaries-are-executable-to-b.patch \
           file://0001-perl-disable-auto-reqs.patch \
           file://0001-rpm-rpmio.c-restrict-virtual-memory-usage-if-limit-s.patch \
           file://0016-rpmscript.c-change-logging-level-around-scriptlets-t.patch \
           file://0001-rpmplugins.c-call-dlerror-prior-to-dlsym.patch \
           file://Unset-D-in-scriptlets.patch \
           \
           file://rpm-4.11.x-siteconfig.patch \
           file://rpm-4.13.0-fedora-specspo.patch \
           file://rpm-4.9.90-no-man-dirs.patch \
           file://rpm-4.8.1-use-gpg2.patch \
           file://rpm-4.12.0-rpm2cpio-hack.patch \
           file://rpm-4.14.1-Add-envvar-that-will-be-present-during-RPM-build.patch \
           file://0001-Document-noverify-in-the-man-page-RhBug-1646458.patch \
           file://0001-Mark-elements-with-associated-problems-as-failed.patch \
           file://0001-Only-read-through-payload-on-verify-if-actually-need.patch \
           file://0003-Verify-packages-before-signing-RhBug-1646388.patch \
           file://0001-Fix-FA_TOUCH-on-files-with-suid-sgid-bits-and-or-cap.patch \
           file://0001-Add-flag-to-use-strip-g-instead-of-full-strip-on-DSO.patch \
           file://0001-Use-in-condition-to-avoid-sub-processes-in-find-debu.patch \
           file://0001-debugedit-Refactor-reading-writing-of-relocated-valu.patch \
           file://0002-Handle-.debug_macro-in-debugedit.patch \
           file://0003-debugedit-Make-sure-.debug_line-old-new-idx-start-eq.patch \
           file://0001-Pass-RPM_BUILD_NCPUS-to-build-scripts.patch \
           file://0001-Use-RPM_BUILD_NCPUS-in-brp-strip-static-archive.patch \
           file://0001-Fix-brp-strip-static-archive-parallelism.patch \
           file://0001-Use-newline-as-a-delimiter-to-avoid-xargs-messing-up.patch \
           file://0001-Make-check-buildroot-check-the-build-files-in-parall.patch \
           file://0001-Fix-resource-leaks-on-zstd-open-error-paths.patch \
           file://0001-Isolate-_smp_build_ncpus-and-use-it-for-_smp_mflags.patch \
           file://rpm-4.14.3-GPG-Switch-back-to-pipe-7-for-signing.patch \
           file://0001-Work-around-buggy-signature-region-preventing-resign.patch \
           file://0001-Fix-python-ts.addErase-not-raising-exception-on-not-.patch \
           file://0001-Always-close-libelf-handle-1313.patch \
           file://0001-When-doing-the-same-thing-more-than-once-use-a-loop.patch \
           file://0001-Introduce-patch_nums-and-source_nums-Lua-variables-i.patch \
           file://0001-Add-limits-to-autopatch-macro.patch \
           file://rpm-4.14.3-bump-up-the-limit-of-signature-header-to-64MB.patch \
           file://rpm-4.14.3-add-fapolicyd-rpm-plugin.patch \
           file://0001-Unblock-signals-in-forked-scriptlets.patch \
           file://rpm-4.14.3-fix-ambiguous-diagnostics-on-file-triggers.patch \
           file://rpm-4.14.3-ELF-files-strip-when-debuginfo-disabled.patch \
           file://0001-In-Python-3-return-all-our-string-data-as-surrogate-.patch \
           file://0001-Return-NULL-string-as-None-from-utf8FromString.patch \
           file://0001-Monkey-patch-.decode-method-to-our-strings-as-a-temp.patch \
           file://0001-Honor-PYTHON-from-configure-when-running-tests.patch \
           file://0002-Use-Python-3-compatible-exception-syntax-in-tests.patch \
           file://0003-Fix-couple-of-bytes-vs-strings-issues-in-Python-test.patch \
           file://0004-Bump-the-minimum-Python-version-requirement-to-2.7.patch \
           file://0005-Drop-an-unnecessary-Python-2-vs-3-incompatibility-fr.patch \
           file://rpm-4.14.3-python3.diff \
           file://rpm-4-14.3-selinux-log-error.patch \
           file://rpm-4.14.2-audit-3.patch \
           file://rpm-4.7.1-geode-i686.patch \
           file://rpm-4.13.90-ldflags.patch \
           file://disable-python-extra.patch \
           "
# This is disabled as it caused issues creating the rootfs:
#         file://rpm-4.14.2-RPMTAG_MODULARITYLABEL.patch


SRC_URI[sha256sum] = "13711268181a6e201eb9d4eb06384a7fcc42dcc6c9057a62c53472cfc5ba44b5"
LIC_FILES_CHKSUM = "file://COPYING;md5=c4eec0c20c6034b9407a09945b48a43f"

S = "${WORKDIR}/rpm-${PV}"
